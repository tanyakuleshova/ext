<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionValTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_val', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('option_id')->nullable();
            $table->unsignedInteger('product_id')->nullable();
            $table->index('option_id');
            $table->index('product_id');
            $table->decimal('add_price', 10, 2)->nullable();
            $table->decimal('add_net_price', 10, 2)->nullable();
            $table->unsignedInteger('bonus')->nullable();
            $table->double('weight')->nullable();
            $table->double('net_weight')->nullable();
            $table->string('reference')->nullable();
            $table->unsignedTinyInteger('active')->nullable();
            $table->unsignedTinyInteger('show_price')->nullable();
            $table->unsignedInteger('sales')->nullable();
            $table->unsignedInteger('refunds')->nullable();
            $table->unsignedInteger('limit')->nullable();
            $table->unsignedInteger('on_stock')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_val');
    }
}
