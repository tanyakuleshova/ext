<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->decimal('price', 10, 2)->nullable();
            $table->decimal('net_price', 10, 2)->nullable();
            $table->unsignedInteger('bonus')->nullable();
            $table->double('weight')->nullable();
            $table->double('net_weight')->nullable();
            $table->string('reference')->nullable();
            $table->unsignedTinyInteger('active')->nullable();
            $table->unsignedTinyInteger('show_price')->nullable();
            $table->unsignedInteger('brand_id')->nullable();
            $table->index(['brand_id']);
            $table->unsignedInteger('category_id')->nullable();
            $table->index(['category_id']);
            $table->unsignedInteger('rating')->nullable();
            $table->unsignedInteger('views')->nullable();
            $table->unsignedInteger('sales')->nullable();
            $table->unsignedInteger('refunds')->nullable();
            $table->unsignedInteger('limit')->nullable();
            $table->unsignedInteger('on_stock')->nullable();

            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
