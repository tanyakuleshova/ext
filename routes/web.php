<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [\App\Http\Controllers\MainController::class, 'index'])->name('home');
Route::get('category/{id}/{link}/{filters?}', [\App\Http\Controllers\CategoryController::class, 'index'])->name('category');
Route::post('category/{id}/{link}/{filters?}', [\App\Http\Controllers\CategoryController::class, 'index']);
Route::get('change_lang/{lang}', [\App\Http\Controllers\LanguageController::class, 'set_lang'])->name('lang');
Route::post('search', [\App\Http\Controllers\SearchController::class, 'product_list']);
