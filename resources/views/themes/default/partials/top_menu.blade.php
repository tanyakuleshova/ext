@foreach($menu as $category)
    <?php $id = uniqid();?>
    @if($category->lang && $category->active)
        <div x-cloak class="relative ml-4">
            <div @click="((show_cat !== 'cat_{{$id}}') || (show_cat == '')) ? (show_cat = 'cat_{{$id}}') : (show_cat = '');" class="cursor-pointer text-gray-600 plain">{!! $category->lang->name !!}</div>
            <div x-show="show_cat === 'cat_{{$id}}'" class="shadow-md absolute z-50" style="width: 300px" x-collapse.duration.500ms>
                @foreach($category->children as $child)
                    <div class="bg-gray-200 px-2">
                        <a class="text-lg text-gray-600" href="{{route('category',['id'=>$child->id, 'link'=>\Illuminate\Support\Str::slug($child->lang->name,'_')])}}">
                            {!! $child->lang->name !!}
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endforeach
