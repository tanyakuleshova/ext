<ul class="p-4 shadow-md">
    @foreach($products as $product)
        <li class="hover:text-blue-500 cursor-pointer">{!! $product->lang->name !!}</li>
    @endforeach
</ul>
