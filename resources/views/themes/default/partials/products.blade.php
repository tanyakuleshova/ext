<div class="">
    <div class="md:flex flex-wrap  md:mx-4 md:justify-end justify-center">
        @foreach($products as $product)
            @if($product->lang)
                <div class="w-64 mx-auto xl:w-1/4 p-4 bg-white md:m-4 my-4 rounded flex flex-col justify-between">
                    <div>
                        <div class="text-center text-md mb-2 plain">{!! $product->lang->name !!}</div>
                        <div class="mx-auto" style="width: 200px">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                                 data-src="{!! $product->lang->link_rewrite !!}" alt="">
                        </div>
                    </div>
                    <div class="text-xl font-bold mt-4">{!! $product->price !!} {{ __('грн') }}</div>
                </div>
            @endif
        @endforeach
    </div>
    <div class="h-48">
        @if(count($products))
            <div class="py-2 px-3 pagination absolute lg:relative right-8 lg:right-auto" style="right: 1rem">
                {{ $products->links('vendor.pagination.tailwind') }}
            </div>
        @endif
    </div>
</div>


