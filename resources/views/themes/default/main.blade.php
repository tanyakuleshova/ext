@extends('themes.default.layout')
@section('content')
    <div class="">
        @include('themes.default.header')

        {!! \App\Services\Outputting\HtmlOutputting::main_banner() !!}
        {!! \App\Services\Outputting\HtmlOutputting::main_menu() !!}

    </div>
@endsection
