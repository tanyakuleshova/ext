<div class="px-4 xl:px-0">
    <div class="flex flex-wrap md:flex-nowrap container mx-auto pt-8 py-4 justify-between items-center max-w-screen-xl m-auto">
        {{-- logo--}}
        <a class="order-1 md:w-auto xl:order-2 border-b-2 border-primary"
           @if(\Illuminate\Support\Facades\Route::current()->getName() != 'home') href="{{route('home')}}" @endif>
            <h3 class="text-2xl text-gray-500 font-bold">the EXTREME shop</h3>
        </a>
        {{-- menu--}}
        <div class="w-full md:w-auto mt-8 xl:mt-0 md:order-2 order-3 font-bold">
{{--            @livewire('client.main-menu')--}}
            <div x-data="{show_cat: ''}" class="flex justify-end lg:text-md flex-wrap">
                {!! \App\Services\Outputting\HtmlOutputting::top_menu() !!}
            </div>
        </div>
        {{-- icons--}}
        <div class="md:order-3 order-2">

                <select onchange="location=this.value" name="" id="" class="cursor-pointer bg-transparent p-1 rounded border border-gray-300 focus:outline-none">
                    <?php $locale = \Illuminate\Support\Facades\App::getLocale(); ?>
                    <option value="{{route('lang', ['lang'=>'uk'])}}" @if($locale == 'uk') selected @endif>uk</option>
                    <option value="{{route('lang', ['lang'=>'en'])}}" @if($locale == 'en') selected @endif>en</option>
                </select>

        </div>
    </div>
    <div class=" xl:flex flex-wrap md:flex-nowrap container mx-auto justify-between items-center text-gray-600 mb-8">
        {{-- slogan--}}
        <div class="hidden xl:block text-gray-600 text-md italic font-bold animate__animated animate__pulse animate__delay-1s">Be free, be yourself</div>

        {{-- search--}}
        <div class="flex items-center w-full mt-4 md:mt-0 md:justify-end md:w-1/3 order-3 md:order-2">
            <div class="relative mx-auto md:mx-0 w-full">
                <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                    <svg class="w-5 h-5 text-primary" fill="currentColor" viewBox="0 0 20 20"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                              clip-rule="evenodd"></path>
                    </svg>
                </div>
                <input onkeyup="search_request(this)" type="text"
                       class="search_input rounded-md py-3 border w-full border-gray-300 border-1 text-gray-900 sm:text-sm focus:ring-gray-300 focus:outline-none block pl-10 p-2"
                       placeholder="Search...">
                <div class="z-50 search_result text-gray-600 text-md bg-gray-100 absolute w-full"></div>
            </div>
        </div>

        {{-- icons--}}
{{--        <div class="flex justify-center items-center order-2 md:order-3">--}}
{{--            @livewire('client.fav-icon', ['fav_qty'=>$fav_qty])--}}
{{--            @livewire('client.cart-icon', ['cart_qty'=>$cart_qty])--}}
{{--        </div>--}}
    </div>
</div>
<script>
    function search_request(_this){
        let q = _this.value;
        if(q.length > 0){
            let res = document.querySelector('.search_result');
            res.innerHTML = '';
            fetch("/search/", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content

                },
                body: JSON.stringify({'q':q })
            })
                .then((response) => response.json())
                .then((response) => {
                    if (response.success) {
                        res.innerHTML = response.view;
                    }
                })
        }
    }

    window.addEventListener('click', function (){
        hide_result();
    });
    function hide_result(){
        let search_result = document.querySelector('.search_result');
        let search_input = document.querySelector('.search_input');
        if (search_input) {
            search_result.innerHTML = '';
            search_input.value = '';
        }
        search_result.addEventListener('click',function (event){
            event.stopPropagation();
        });
        search_input.addEventListener('click',function (event){
            event.stopPropagation();
        });
    }

</script>
