<?php

namespace App\Services\Outputting;

use App\Repository\CategoryRepository;
use App\Services\Caching\RedisCaching;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;

class HtmlOutputting
{
    public static function top_menu(): string
    {
        return self::get_menu("top_menu");
    }

    public static function main_menu(): string
    {
        return self::get_menu("main_menu");
    }

    public static function get_menu($type): string
    {
        $menu = Redis::get($type."_" . App::getLocale());
        if($menu) return $menu;

        $menu =  (new CategoryRepository)->menu_categories();
        $view = view('themes/'.config('customize.theme').'/partials/'.$type, compact('menu'))->render();
        Redis::set($type."_" . App::getLocale(), $view);

        return $view;
    }

    public static function main_banner(): string
    {
        $view = Redis::get("main_banner_" . App::getLocale());
        if($view) return $view;

        $view = view('themes/'.config('customize.theme').'/partials/main_banner')->render();
        Redis::set("main_banner_" . App::getLocale(), $view);

        return $view;
    }
}
