<?php
namespace App\Services\Translator;

use Dejurin\GoogleTranslateForFree;

class TranslatorNLP implements TranslatorInterface
{
    public function translate($lang_from, $text, $lang_to)
    {
        $tr = new GoogleTranslateForFree();
        return $tr->translate($lang_from, $lang_to, $text, 5);
    }
}
