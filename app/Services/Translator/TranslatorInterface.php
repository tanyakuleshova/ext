<?php
namespace App\Services\Translator;

interface TranslatorInterface
{
    public function translate($lang_from, $text, $lang_to);
}
