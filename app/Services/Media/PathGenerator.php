<?php

namespace App\Services\Media;

use Spatie\MediaLibrary\MediaCollections\Models\Media;
use function config;

class PathGenerator implements \Spatie\MediaLibrary\Support\PathGenerator\PathGenerator
{
    /*
     * Get the path for the given media, relative to the root storage path.
     */
    public function getPath(Media $media): string
    {
        $folder = str_pad(substr($media->getKey(), -2), 2, '0', STR_PAD_LEFT);
        return 'media/'.$folder .'/'.$media->getKey().'/';
    }

    /*
     * Get the path for conversions of the given media, relative to the root storage path.
     */
    public function getPathForConversions(Media $media): string
    {
        $folder = str_pad(substr($media->getKey(), -2), 2, '0', STR_PAD_LEFT);
        return 'media/'.$folder .'/'.$media->getKey().'/size/';
//        return config('variables.image_dir') . config('variables.image_folder') .'media/'.$this->getBasePath($media).'/conversions/';
    }

    /*
     * Get the path for responsive images of the given media, relative to the root storage path.
     */
    public function getPathForResponsiveImages(Media $media): string
    {
        $folder = str_pad(substr($media->getKey(), -2), 2, '0', STR_PAD_LEFT);
        return 'media/'.$folder .'/'.$media->getKey().'/responsive-images/';
//        return $this->getBasePath($media).'/responsive-images/';
    }

    /*
     * Get a unique base path for the given media.
     */
    protected function getBasePath(Media $media): string
    {
        $prefix = config('media-library.prefix', '');

        if ($prefix !== '') {
            return $prefix . '/' . $media->getKey();
        }

        return $media->getKey();
    }
}
