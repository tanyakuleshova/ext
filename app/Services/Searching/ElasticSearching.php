<?php
namespace App\Services\Searching;

use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Support\Arr;

class ElasticSearching implements SearchingInterface
{
    protected $elasticsearch;

    public function __construct()
    {
        $this->elasticsearch = ClientBuilder::create()->setHosts([env('ELASTIC_HOST')])->build();
    }
    public function product_search($query): array
    {

        $params = [
            'index' => 'product',
            'body' => [
                'size' => 500,
                'query' => [
                    'wildcard' => [
                        "langs.name" => [
                            "value" => "*$query*"
                        ]
                    ]
                ]
            ]
        ];
        $results = $this->elasticsearch->search($params)['hits']['hits'] ?? [];
        return Arr::pluck($results, '_id');
    }
}
