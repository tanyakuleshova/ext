<?php
namespace App\Services\Searching;

interface SearchingInterface
{
    public function product_search($query) :array;
}
