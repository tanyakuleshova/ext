<?php

namespace App\Services\Filtering;

use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class ElasticFiltering implements FilteringInterface
{
    protected $elasticsearch;

    public function __construct()
    {
        $this->elasticsearch = ClientBuilder::create()->setHosts([env('ELASTIC_HOST')])->build();
    }

    public function getProductFilters($category_id): array
    {
        $params = [
            'index' => 'product',
            'body' => [
                'size' => 0,
                'query' => [
                    'bool' => [
                        "must" => [
                                ["match" => ["category_id" => $category_id]]
                        ]
                    ]
                ],
                'aggs'=>[
                    'distinctKeys'=>[
                        'terms'=>[
                            'field' => "attr.".App::getLocale().".id",
                            'size'=>500
                        ]
                    ],
                    'distinctValues'=>[
                        'terms'=>[
                            'field' => "attr.".App::getLocale().".name.keyword",
                            'size'=>500
                        ]
                    ]
                ]
            ]
        ];
        $attributes_values = $this->elasticsearch->search($params)['aggregations']['distinctValues']['buckets'] ?? [];
        $attributes_keys = $this->elasticsearch->search($params)['aggregations']['distinctKeys']['buckets'] ?? [];

        foreach ($attributes_keys as $k => $key){
            $params = [
                'index' => 'product',
                'body' => [
                    'size' => 0,
                    'query' => [
                        'bool' => [
                            "must" => [
                                ["match" => ["category_id" => $category_id]]
                            ]
                        ]
                    ],
                    'aggs'=>[
                        'distinctKeys'=>[
                            'terms'=>[
                                'field' => "atr_".$key['key'].".value_id",
                                'size'=>500
                            ]
                        ],
                        'distinctValues'=>[
                            'terms'=>[
                                'field' => "atr_".$key['key'].".value.".App::getLocale().".keyword",
                                'size'=>500
                            ]
                        ]
                    ]
                ]
            ];
            $inner_value = $this->elasticsearch->search($params)['aggregations']['distinctValues']['buckets'] ?? [];
            $inner_keys = $this->elasticsearch->search($params)['aggregations']['distinctKeys']['buckets'] ?? [];
            $attributes_values[$k]['atr_id'] = $key['key'];

            $key_value = [];
            foreach ($inner_value as $key => $value){
                $key_value[$inner_keys[$key]['key']]['name'] = $value['key'];
                $key_value[$inner_keys[$key]['key']]['doc_count'] = $value['doc_count'];
                $attributes_values[$k]['values'] = $key_value;
            }
        }
//        dd($attributes_values);
        return $attributes_values;
    }

    function getFilteredProducts($applied_filters, $category_id): array
    {
        $query_params = $this->prepare_data($applied_filters, $category_id);

        $params = [
            'index' => 'product',
            'body' => [
                'size' => 500,
                'query' => [
                    'bool' => [
                        'should' => $query_params
                    ]
                ]
            ]
        ];
//        dd($query_params);
        $filtered_products = $this->elasticsearch->search($params)['hits']['hits'] ?? [];
        return Arr::pluck($filtered_products, '_id');
    }

    function updateFilters($all_filters, $applied_filters, $category_id): array
    {
        $atr_values = $all_filters;
        foreach ($atr_values as $k => $atr_value){
            if(($atr_value['atr_id'] ?? '')){
                if($this->filter_exist($applied_filters, $atr_value['atr_id'])){
                    $filters = $this->remove_filter($applied_filters, $atr_value['atr_id']);
                    $query_params =  $this->prepare_data($filters, $category_id);
                }else{
                    $query_params =  $this->prepare_data($applied_filters, $category_id);
                }


                $params = [
                    'index' => 'product',
                    'body' => [
                        'size' => 0,
                        'query' => [
                            'bool' => [
                                'should' => $query_params
                            ]
                        ],
                        'aggs'=>[
                            'distinctKeys'=>[
                                'terms'=>[
                                    'field' => "atr_".$atr_value['atr_id'].".value_id",
                                    'size'=>500
                                ]
                            ]
                        ]
                    ]
                ];
//                dd($params);

                $keys = $this->elasticsearch->search($params)['aggregations']['distinctKeys']['buckets'];

//                dd($keys);
                foreach ($all_filters[$k]['values'] as $i=>$v){
                    $all_filters[$k]['values'][$i]['doc_count'] = 0;
                }
                foreach ($keys as $item){
                    $all_filters[$k]['values'][$item['key']]['doc_count'] = $item['doc_count'];
                }
            }
        }
        return $all_filters;
    }

    public function renderCombinations($applied_filters): array
    {
        $or_block = [];
        $block['must'] = [];
        $block['should'] = [];
        $or_block[] = $block;

        usort($applied_filters, function ($a, $b) {
            return (count($b['value_ids']) - count($a['value_ids']));
        });

        foreach ($applied_filters as $applied_filter) {
            $count = count($applied_filter['value_ids']);
            if ($count == 1) {
                for ($i = 0; $i < count($or_block); $i++) {
                    $or_block[$i]['must'][] = ["match" => ["atr_" . $applied_filter['atr_id'] . ".value_id" => $applied_filter['value_ids'][0]]];
                }
            } elseif ($count > 1) {
                if (count(end($or_block)['should']) > 0) {
                    $blocks = [];
                    for ($i = 0; $i < count($or_block); $i++) {
                        foreach ($applied_filter['value_ids'] as $k => $value) {
                            $block = $or_block[$i];
                            $block['must'][] = ["match" => ["atr_" . $applied_filter['atr_id'] . ".value_id" => $value]];
                            $blocks[] = $block;
                        }
                    }
                    $or_block = $blocks;
                } else {
                    foreach ($applied_filter['value_ids'] as $value) {
                        $or_block[0]['should'][] = ["match" => ["atr_" . $applied_filter['atr_id'] . ".value_id" => $value]];
                    }

                }
            }
        }
        return $or_block;
    }

    public function formatDataForRequest($or_block, $id): array
    {
        $query_params = [];
        foreach ($or_block as $block) {
            $must = $block['must'];
            $must[] = ["match" => ["category_id" => $id]];
            $must[] = [
                "bool" => [
                    "should" => $block['should']
                ]
            ];
            $bool = [];
            $bool['bool']['must'] = $must;
            $query_params[] = $bool;
        }
        return $query_params;
    }

    public function prepare_data($applied_filters, $id): array
    {
        $combinations = $this->renderCombinations($applied_filters);
        return $this->formatDataForRequest($combinations, $id);
    }

    function filter_exist($applied_filters, $atr_id): bool
    {
        foreach ($applied_filters as $filter){
            if($filter['atr_id'] == $atr_id) return true;
        }
        return false;
    }

    function remove_filter($applied_filters, $atr_id): array
    {
        $filters = [];
        foreach ($applied_filters as $filter){
            if($filter['atr_id'] != $atr_id){
                $filters[] = $filter;
            }
        }
        return $filters;
    }
}
