<?php

namespace App\Services\Filtering;

interface FilteringInterface
{
    public function getProductFilters($category_id): array;
    public function getFilteredProducts($applied_filters, $category_id);
    public function updateFilters($all_filters, $applied_filters, $category_id): array;
}
