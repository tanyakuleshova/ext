<?php
namespace App\Services\Parsing;

interface ParserInterface
{
    public function start();
}
