<?php
namespace App\Services\Parsing;

use App\Models\Attribute;
use App\Models\AttributeDesc;
//use App\Models\AttributeVal;
//use App\Models\AttributeValDesc;
use App\Models\AttributeValue;
use App\Models\AttributeValueDesc;
use App\Models\Brand;
use App\Models\Category;
use App\Models\CategoryDesc;
use App\Models\Product;
use App\Models\ProductDesc;
use App\Repository\ProductRepository;
use Elastic\Elasticsearch\ClientBuilder;
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

class Parser implements ParserInterface
{
    public Client $client;
    public $elasticsearch;
    public string $category;
    public int $page = 1;
    public array $products = [];

    public function __construct($category)
    {
        $this->client = new Client(HttpClient::create(['verify_peer' => false, 'verify_host' => false]));
        $this->category = $category;
        $this->elasticsearch = ClientBuilder::create()->build();
    }

    public function start()
    {
        try{
            $category = $this->category. "?page=".$this->page;
            $selector = '.product-block-inner .image a';
            $crawler = $this->client->request('GET', $category);

            dump($category);
            $this->products = [];

            $crawler->filter($selector)->each(function ($node){
                $content = $node->attr('href');
                $this->products[] = $content;
            });

            $result = $this->work();
            if($result) return $result;

            $this->page += 1;
            $this->start();


        }catch (\Exception $e){
            dump($e->getMessage());
        }
    }

    public function work()
    {
        if(empty($this->products)){
            return 1;
        }
        $this->products = array_unique($this->products);

        foreach ($this->products as $product){

            dump($product);

            $data = $this->client->request('GET', $product);
            $this->save($data);

        }
        $this->products = [];
    }

    public function save($crawler)
    {
        $selector = '#tab-params > ul > li';
        $name = '.page-title';
        $reference = '.detail-code .detail-code-i';
        $price = '.new-price span[itemprop="price"]';
        $description = '#tab-description .description__wrapper';
        $image = '.slider-for-card .image-block img';
        $bc = '.breadcrumb > li';

        $image_link = $crawler->filter($image)->each(function ($node) {
            return $node->attr('data-blazy-lazy');
        })[0] ?? '';

        $image_link = str_replace('.jpg', '.webp', $image_link);
        $image_link = str_replace('https://motostyle.ua/', '', $image_link);


        $characteristics = $crawler->filter($selector)->each(function ($node) {
            return $node->text();
        });

        //save product
        $product = new Product();

        $product->reference = $crawler->filter($reference)->each(function ($node) {
                return $node->text();
            })[0] ?? '';

        $price = $crawler->filter($price)->each(function ($node) {
            return $node->text();
        })[0] ?? '';

        $product->price  = preg_replace('/\s+/', '', $price);

        $product->brand_id = 1;
        $product->category_id = 1;
        $product->active = 1;
        $product->show_price = 1;
        $product->save();

        //save category
        $breadcrumbs = $crawler->filter($bc)->each(function ($node) {
            return $node->text();
        });

        $last_category = 1;
        foreach ($breadcrumbs as $k => $breadcrumb){
            if ($k > 0 && $k < (count($breadcrumbs)-1)){
                $category_desc = CategoryDesc::where('name', $breadcrumb)->first();
                if(!$category_desc){

                    $category = new Category();
                    $category->parent_id = 1;
                    $category->level_depth = 1;
                    if($k>1){
                        $parent_desc = CategoryDesc::where('name', $breadcrumbs[$k-1])->first();
                        if($parent_desc){
                            $category->level_depth = $parent_desc->category->level_depth + 1;
                            $category->parent_id = $parent_desc->category_id;
                        }
                    }
                    $category->active = 1;
                    $category->save();

                    $category_desc = new CategoryDesc();
                    $category_desc->category_id = $category->id;
                    $category_desc->lang = 'ru';
                    $category_desc->name = $breadcrumb;
                    $category_desc->save();
                }
                $last_category = $category_desc->category_id;
            }
        }
        $product->category_id = $last_category;
        $product->save();

        //save attributes
        foreach ($characteristics as $k => $c) {
            $arr = explode(':', $c);

            if (count($arr) == 2) {
                $atr_desc = AttributeDesc::where('name', $arr[0])->first();
                if (!$atr_desc) {
                    $atr = new Attribute();
                    $atr->position = $k;
                    $atr->save();

                    $atr_desc = new AttributeDesc();
                    $atr_desc->attribute_id = $atr->id;
                    $atr_desc->name = trim($arr[0]);
                    $atr_desc->lang = 'ru';
                    $atr_desc->save();
                }
                $attr_val = AttributeValue::where('product_id', $product->id)->where('attribute_id', $atr_desc->attribute_id)->first();
                if (!$attr_val) {
                    $attr_val = new AttributeValue();
                    $attr_val->product_id = $product->id;
                    $attr_val->attribute_id = $atr_desc->attribute_id;
                    $attr_val->save();

                    $atr_val_desc = new AttributeValueDesc();
                    $atr_val_desc->name = trim($arr[1]);
                    $atr_val_desc->lang = 'ru';
                    $atr_val_desc->attribute_val_id = $attr_val->id;
                    $atr_val_desc->save();
                }
                //save brand
                if ($arr[0] == 'Бренд') {
                    $brand = Brand::where('name', $arr[1])->first();
                    if (!$brand) {
                        $brand = new Brand();
                        $brand->name = trim($arr[1]);
                        $brand->save();
                        $product->brand_id = $brand->id;
                        $product->save();
                    }
                }
            }

        }

        //save product desc
        $desc = new ProductDesc();
        $desc->product_id = $product->id;
        $desc->lang = 'ru';
        $desc->link_rewrite = $image_link;
        $desc->name = $crawler->filter($name)->each(function ($node) {
            return $node->text();
        })[0] ?? '';

        $desc->description = $crawler->filter($description)->each(function ($node) {
            return $node->text();
        })[0] ?? '';
        $desc->save();

//        $this->elasticsearch->index([
//            'index' => 'product',
//            'type' => '_doc',
//            'id' => $product->id,
//            'body' =>  (new ProductRepository)->instance($product->id)
//        ]);
    }
}
