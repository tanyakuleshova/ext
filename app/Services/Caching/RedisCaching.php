<?php
namespace App\Services\Caching;

use App\Models\Category;
use App\Repository\CategoryRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;

class RedisCaching implements CachingInterface
{
    public static function getMenu()
    {
        $menu = Redis::get("menu_" . App::getLocale());
        if ($menu) {
            return json_decode($menu);
        } else {
            $menu =  (new CategoryRepository)->menu_categories();
            Redis::set("menu_" . App::getLocale(), json_encode($menu));
        }

        return $menu;

    }

}
