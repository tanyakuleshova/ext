<?php

namespace App\Providers;

use App\Repository\CategoryRepository;
use App\Services\Filtering\ElasticFiltering;
use App\Services\Filtering\ElasticFilteringUpdated;
use App\Services\Filtering\FilteringInterface;
use App\Services\Searching\ElasticSearching;
use App\Services\Searching\SearchingInterface;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Client::class, function(){
            return ClientBuilder::create()
                ->setHosts([env('ELASTIC_HOST')])
                ->build();
        });

        $this->app->bind(
            FilteringInterface::class,
            function () {
//                return new ElasticFiltering();
                return new ElasticFilteringUpdated();
            }
        );

        $this->app->bind(
            SearchingInterface::class,
            function () {
                return new ElasticSearching();
            }
        );

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
