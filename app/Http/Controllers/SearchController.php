<?php

namespace App\Http\Controllers;

use App\Repository\ProductRepository;
use App\Services\Searching\SearchingInterface;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function __construct(public SearchingInterface $searchService, public ProductRepository $productRepository){}

    public function product_list()
    {
        $q = \request()->get('q');
        $product_ids = $this->searchService->product_search($q);
        if(!empty($product_ids)){
            $products = $this->productRepository->getByIds($product_ids, 15);
            $view = view('themes/'.config('customize.theme').'/partials/search_results_ajax', compact( 'products'))->render();
            return response(['success'=>true, 'view'=>$view]);
        }
        return response(['success'=>false]);
    }
}
