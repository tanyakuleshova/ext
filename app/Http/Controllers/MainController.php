<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Repository\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;

class MainController extends Controller
{
    public function index()
    {

        $menu =  (new CategoryRepository)->menu_categories();
//        dd(App::getLocale());
        return view('themes/'.config('customize.theme').'/main', compact('menu'));
    }
}
