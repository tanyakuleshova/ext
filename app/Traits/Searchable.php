<?php
namespace App\Traits;

use App\Observers\ElasticSearchObserver;

trait Searchable
{
    public static function bootSearchable(): void
    {
        static::observe(ElasticSearchObserver::class);
    }

    public function getSearchIndex(): string
    {
        return $this->getTable();
    }

    public function getSearchType(): string
    {
        return '_doc';
    }
}
