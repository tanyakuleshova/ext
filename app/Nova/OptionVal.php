<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Http\Requests\NovaRequest;
use Yassi\NestedForm\NestedForm;

class OptionVal extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\OptionVal::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];
    public static function label()
    {
        return __('Product Options Values');
    }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('name', function ($model){
                return $model->desc
                    ? Str::limit($model->desc->name, 30, '...') : '';
            }),
            Select::make('Product', 'product_id')->options((new \App\Models\Product())->options())->hideFromDetail()->hideFromIndex(),

            Text::make(__('Reference'),'reference'),

            Select::make('Option Name', 'option_id')->options((new \App\Models\Option())->options())->hideFromDetail()->hideFromIndex(),

            Number::make(__('Net price'),'add_net_price')->sortable(),
            Number::make(__('Price'),'add_price')->sortable(),
            Number::make(__('Bonus'),'bonus')->hideFromIndex(),

            Number::make(__('Net Weight'),'net_weight')->hideFromIndex(),
            Number::make(__('Weight'),'weight')->hideFromIndex(),

            Boolean::make(__('Active'),'active')->sortable(),
            Boolean::make(__('Show Price'),'show_price')->hideFromIndex(),
            Number::make(__('Limit'),'limit')->hideFromIndex(),
            Number::make(__('On stock'),'on_stock')->hideFromIndex(),

            Images::make('Images', 'options')
                ->withResponsiveImages()

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
