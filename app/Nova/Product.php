<?php

namespace App\Nova;


//use App\Service\ImageService;
use Chaseconey\ExternalImage\ExternalImage;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasManyThrough;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Http\Requests\NovaRequest;
use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use Nette\Utils\Html;
use Yassi\NestedForm\NestedForm;

class Product extends Resource
{
    public static $model = \App\Models\Product::class;
    public static $title = 'id';
    public static function label()
    {
        return __('Products');
    }

    public static $search = [
        'id', 'reference',
//        'l.name', 'cl.name'
    ];

    public function subtitle()
    {
        parent::subtitle();
//        return $this->lang->name ?? '';
    }

    public function fields(Request $request)

    {
//        dd((new \App\Models\Category())->options());
//        $editable = (str_contains($request->path(), 'update-fields') || str_contains($request->path(), 'creation-fields'));
//        $options =
//        $folder = str_pad(mt_rand(0,100), 2, '0', STR_PAD_LEFT);
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make('attr', function ($model){
                $str = '';
                foreach ($model->attributes as $attribute_value){
                    $str .= $attribute_value->value->attribute->lang->name . " ". $attribute_value->value->lang->name . " ";
                }
                return $str;
            })->hideFromIndex(),
            Text::make('name', function ($model){
                return $model->lang
                    ? Str::limit($model->lang->name, 30, '...') : '';
            }),
            Text::make('Category', function ($model){
                return   ($model->category && $model->category->lang) ? $model->category->lang->name : '';
            }),
            Trix::make('description', function ($model){
                return $model->lang
                    ? Str::limit($model->lang->description, 30, '...') : '';
            }),
            Text::make(__('Reference'),'reference'),
            Select::make('Category', 'category_id')->options((new \App\Models\Category())->options())->hideFromDetail()->hideFromIndex(),


//            Text::make( 'category', function ($model){
//                return ($model->category && $model->category->lang)
//                    ? Str::limit($model->category->lang->name, 30, '...') : "";
//            }),

//            NestedForm::make('ProductDesc', 'langs', 'App\Nova\ProductDesc')->heading('Description '.NestedForm::wrapIndex()),
//            NestedForm::make('AttributeVal', 'attribute_vals', 'App\Nova\AttributeVal')->heading('Attribute '.NestedForm::wrapIndex()),

//            NestedForm::make('Brand', 'brand', 'App\Nova\Brand')->heading('Brand '.NestedForm::wrapIndex()),

            Number::make(__('Net price'),'net_price')->sortable(),
            Number::make(__('Price'),'price')->sortable(),
            Number::make(__('Bonus'),'bonus')->hideFromIndex(),

            Number::make(__('Net Weight'),'net_weight')->hideFromIndex(),
            Number::make(__('Weight'),'weight')->hideFromIndex(),

            Boolean::make(__('Active'),'active')->sortable(),
            Boolean::make(__('Show Price'),'show_price')->hideFromIndex(),
            Number::make(__('Limit'),'limit')->hideFromIndex(),
            Number::make(__('On stock'),'on_stock')->hideFromIndex(),


//            Image::make('pic', function($model){
//                return $model->lang ? $model->lang->link_rewrite : '';
//            })->disk('external'),
            Images::make('Images', 'collection')
                ->withResponsiveImages(),

            HasMany::make('ProductDesc','langs', 'App\Nova\ProductDesc'),
//            HasMany::make('OptionVal','option_vals', 'App\Nova\OptionVal'),

//            HasMany::make('AttributeVal', 'attribute_vals', 'App\Nova\AttributeVal'),
//            BelongsTo::make('Brand', 'brand', 'App\Nova\Brand'),
//            MorphMany::make('Tag', 'tags', 'App\Nova\Tag')
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
