<?php

namespace App\Nova;


use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Yassi\NestedForm\NestedForm;

class Category extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Category::class;
    public static function label()
    {
        return __('Categories');
    }
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

//        $options = (str_contains($request->path(), 'update-fields') || str_contains($request->path(), 'creation-fields')) ? (CategoryRepository::categoriesTree()) :  ['0' => $request->path()];

        return [
            ID::make(__('ID'), 'id')->sortable(),

            Select::make(__('Parent'), 'parent_id')->searchable()->options((new \App\Models\Category)->options())->hideFromIndex(),
            Text::make('name', function ($model){
              return $model->lang->name ?? '';
            }),
            Text::make('parent', function ($model){
                return $model->parent->lang->name ?? '';
            }),
            Text::make('lang', function (){
                return App::getLocale();
            }),
            Boolean::make('Active')->sortable(),
//            HasMany::make('Product', 'products', 'App\Nova\Product'),

            NestedForm::make('CategoryDesc', 'langs', 'App\Nova\CategoryDesc')->heading('Description '.NestedForm::wrapIndex()),

            Images::make('Images', 'collection')
                ->withResponsiveImages(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
