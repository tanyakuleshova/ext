<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagDesc extends Model
{
    public $table = 'tag_desc';
    public $primaryKey = 'id';
    public $guarded = [];

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
