<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Option extends Model
{

    public $table = 'option';
    public $primaryKey = 'id';
    public $guarded = [];

    public function lang()
    {
        return $this->hasOne(OptionDesc::class, 'option_id', 'id')
            ->where('lang', App::getLocale());
    }

    public function langs()
    {
        return $this->hasMany(OptionDesc::class, 'option_id', 'id');
    }

    public function options()
    {
        $options = [];
        foreach (OptionDesc::where('lang', App::getLocale())->get() as $lang){
            $options[$lang->option_id] = $lang->name;
        }
        return $options;
    }
}
