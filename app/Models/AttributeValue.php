<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class AttributeValue extends Model
{
    public $table = 'attribute_value';
    public $primaryKey = 'id';
    public $guarded = [];

    public function attribute()
    {
        return $this->hasOne(Attribute::class, 'id', 'attribute_id');
    }

    public function lang()
    {
        return $this->hasOne(AttributeValueDesc::class, 'attribute_val_id', 'id')
            ->where('lang', App::getLocale());
    }

    public function langs()
    {
        return $this->hasMany(AttributeValueDesc::class, 'attribute_val_id', 'id');
    }
}
