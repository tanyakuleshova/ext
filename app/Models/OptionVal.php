<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class OptionVal extends Model  implements HasMedia
{
    use InteractsWithMedia;

    public $table = 'option_val';
    public $primaryKey = 'id';
    public $guarded = [];

    public function lang()
    {
        return $this->hasOne(OptionDesc::class, 'option_id', 'option_id')
            ->where('lang', App::getLocale());
    }

    public function langs()
    {
        return $this->hasMany(OptionDesc::class, 'option_id', 'option_id');
    }
    public function option()
    {
        return $this->belongsTo(Option::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function registerMediaConversions(Media $media = null):void
    {
        $this->addMediaConversion('xs')
            ->width(100)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('sm')
            ->width(200)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('md')
            ->width(600)
            ->format('webp')
            ->nonQueued();

    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('main')->singleFile();
        $this->addMediaCollection('collection');
    }
}
