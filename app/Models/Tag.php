<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Tag extends Model
{
    public $table = 'tag';
    public $primaryKey = 'id';
    public $guarded = [];

    public function name()
    {
        return $this->hasOne(TagDesc::class, 'tag_id', 'id')
            ->where('lang', App::getLocale());;
    }

    public function values()
    {
        return $this->hasMany(TagDesc::class);
    }
    public function products()
    {
        return $this->morphedByMany(Product::class, 'taggable');
    }
}
