<?php

namespace App\Console\Commands;

use App\Models\Attribute;
use App\Models\AttributeDesc;
use App\Models\AttributeProduct;
use App\Models\AttributeValue;
use App\Models\AttributeValueDesc;
use App\Models\Category;
use App\Models\CategoryDesc;
use App\Models\Product;
use App\Models\ProductDesc;
use Illuminate\Console\Command;

class get_data extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $this->getProducts();
        } catch (\Exception $e) {
            dump($e->getMessage());
        }

    }

    public function make_request($endpoint)
    {

        $headers = [
            "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2ODM1MjgyODksImV4cCI6MTY4MzYxNDY4OSwicm9sZXMiOlsiUk9MRV9DTElFTlRfTUFJTiJdLCJsb2dpbiI6ImRyaWZ0MjFAYmlnbWlyLm5ldCJ9.t-HndLUe1CLXGY8z2CPbcKJGMRllJrrlvrGXRgUE4uZ5_5iZ-3JMfuHXGJjicqiC-8_LpXnUc_w4r7xvY6-FOvPJYrxBUufhMvUW63fGyTMvsUKW8AJ_UiEiGaOx3sADLtyi5DpgX81DLewAD7NoJLrvmN4NR9eEywLoZGmHOZDfMhtLsIlgXXpJTTwNyeVBKb5IGJ06LaXGkXV6VqjkUEklylfwG_Rig4jwh2jNb3-z-Oj2ytdiN7LN3n2pYgc4WlifqU4uSTGhcdkH8hwm4FQXs9f7J4Aq_MoQtEyKz78tnru_90TaABu1Kvx9QUBv7SQ4wsEvpwFF25dw1NqLcbNIM5Cq4R7zeD_yQiyQg0UI2rpPGmLuvR5P5KsO5B7kSyoYJdjXENj8I_9e-sOy60sxVVBOqVguNQGeKVeJORWxLTRfZzwdfbOjMSdP8jV4_soZlT0rwEzhh9ywfTSWLK7tYIgkCbzCBag512_1NIV-57bbVkawscpglh9VnfWHBGDwl3MOssHuvTpR5Q2wn5YfpIV0DFkcgatwXi15wSKvGQ-d48Sav6PJl-jVuft2RZBTaprHBC6xiEKDCG_yguTkt2t7uB2PZONxdV_a4EeSVrVVjK6LFElEVIQyn2hSuLb8E8XoT95jQ5Qsc-ng-UY8bSl_EH6OLfcHQRQ8bTA",
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $output = curl_exec($ch);
        curl_close($ch);


        return json_decode($output);
    }

    public function getProducts()
    {
        $categories = Category::where('parent_id', '!=', 0)->get();
        foreach ($categories as $category) {
            if ($category->id == 4) {
                dump($category->id);
                $pagination = $this->make_request("https://order24-api.utr.ua/catalog/akeneo/productsByPagination?type=bearing&page=1&limit=20&categories=" . $category->id);

                dump($pagination);
                $totalPages = ceil($pagination->pagination->totalCount / $pagination->pagination->numberPerPage);

                if ($totalPages > 0) {
                    for ($i = 1; $i <= $totalPages; $i++) {

                        $page = $this->make_request("https://order24-api.utr.ua/catalog/akeneo/productsByPagination?type=antifreeze&page=$i&limit=20&categories=" . $category->id);
                        $products = $page->details ?? [];

                        foreach ($products as $product) {

                            //save product
                            $newProduct = Product::where('id', $product->id)->first();

                            if (!$newProduct) {
                                $newProduct = new Product();
                                $newProduct->id = $product->id;
                                $newProduct->category_id = $category->id;
                                $newProduct->price = $product->yourPrice ? $product->yourPrice->amount ?? 0 : 0;
                                $newProduct->reference = $product->article;
                                $newProduct->active = 1;
                                $newProduct->show_price = 1;
                                $newProduct->save();

                                $productDesc = new ProductDesc();
                                $productDesc->product_id = $newProduct->id;
                                $productDesc->lang = 'ru';
                                $productDesc->name = $product->title;
                                $productDesc->link_rewrite = $product->images ? ($product->images[0]?->fullImagePath ?? "") : "";
                                $productDesc->save();

                                //save brand
                                if ($product->brand && $product->brand->name) {
                                    $brandName = $product->brand->name;
                                    $brandID = $product->brand->id;
                                    $this->saveAttribute(1, 1, "Бренд", $brandName, $product->id);
                                }

                                //save attributes
                                if ($product->detailAkeneoInfo ?? '') {
                                    foreach ($product->detailAkeneoInfo as $attributeResponse) {
                                        $atr_value = $attributeResponse->value ?? $attributeResponse->values[0] ?? "";

                                        if ($atr_value) {

                                            $atr_id = $attributeResponse->attribute->id;
                                            $sort = $attributeResponse->attribute->sort_order;
                                            $atr_title = $attributeResponse->attribute->title;
                                            try {
                                                if (isset($attributeResponse->attribute->unit) && isset($attributeResponse->attribute->unit->measure)) {
                                                    $atr_value = $atr_value . " " . $attributeResponse->attribute->unit->measure;
                                                }
                                            } catch (\Exception $e) {
                                                dump($e->getMessage());
                                            }

                                            $this->saveAttribute($atr_id, $sort, $atr_title, $atr_value, $product->id);

                                        }
                                    }
                                }


                            }
                            dump($product->id);
                        }
                    }
                }
            }
        }
    }

    public function saveAttribute($atr_id, $sort, $atr_title, $atr_value, $product_id)
    {
        $attributeLang = AttributeDesc::where('name', $atr_title)->first();
        if (!$attributeLang) {

            $attribute = new Attribute();
            $attribute->id = $atr_id;
            $attribute->position = $sort;
            $attribute->save();

            $attributeLang = new AttributeDesc();
            $attributeLang->attribute_id = $atr_id;
            $attributeLang->name = $atr_title;
            $attributeLang->lang = 'ru';
            $attributeLang->save();
        }

//        $attribute = Attribute::where('id', $atr_id)->first();
//        if (!$attribute) {
//            $attribute = new Attribute();
//            $attribute->id = $atr_id;
//            $attribute->position = $sort;
//            $attribute->save();
//        }

        $atr_id = $attributeLang->attribute_id;

        $attributeValueLang = AttributeValueDesc::where('name', $atr_value)->where('attribute_id', $atr_id)->first();
        if (!$attributeValueLang) {
            $attributeValue = new AttributeValue();
            $attributeValue->attribute_id = $atr_id;
            $attributeValue->save();

            $attributeValueLang = new AttributeValueDesc();
            $attributeValueLang->attribute_id = $atr_id;
            $attributeValueLang->attribute_val_id = $attributeValue->id;
            $attributeValueLang->lang = "ru";
            $attributeValueLang->name = $atr_value;
            try {
                $attributeValueLang->save();
            } catch (\Exception $e) {
                $attributeValueLang->name = json_encode($atr_value);
                $attributeValueLang->save();
            }

        }

        $attributeProduct = new AttributeProduct();
        $attributeProduct->product_id = $product_id;
        $attributeProduct->attribute_val_id = $attributeValueLang->attribute_val_id;
        $attributeProduct->save();
    }

    public function getCategories()
    {
        $categories = $this->make_request("https://order24-api.utr.ua/catalog/akeneo/categories");
        foreach ($categories as $category) {

            $cat = new Category();
            $cat->id = $category->id;
            $cat->active = 1;
            $cat->parent_id = $category->parentId ?? 0;
            $cat->level_depth = 0;
            $cat->save();

            $category_desc = new CategoryDesc();
            $category_desc->category_id = $cat->id;
            $category_desc->lang = "ru";
            $category_desc->name = $category->title;
            $category_desc->save();

            if ($category->children) {
                foreach ($category->children as $children) {
                    $cat = new Category();
                    $cat->id = $children->id;
                    $cat->active = 1;
                    $cat->parent_id = $children->parentId ?? 0;
                    $cat->level_depth = 1;
                    $cat->save();

                    $category_desc = new CategoryDesc();
                    $category_desc->category_id = $cat->id;
                    $category_desc->lang = "ru";
                    $category_desc->name = $children->title;
                    $category_desc->save();
                }
            }
        }
    }
}
