<?php

namespace App\Console\Commands;


use App\Models\AttributeDesc;
use App\Models\AttributeValueDesc;
use App\Models\Brand;
use App\Models\CategoryDesc;
use App\Models\Product;
use App\Models\ProductDesc;
use App\Services\Translator\TranslatorNLP;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class UpdateIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Product update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $pr = Product::all();
        foreach ($pr as $p){
            $p->save();

        }

    }

}
