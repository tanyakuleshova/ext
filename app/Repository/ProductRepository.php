<?php

namespace App\Repository;

use App\Models\Product;
use App\Repository\Formatting\ELProduct;

class ProductRepository extends AbstractRepository
{
    use ELProduct;

    const MODEL = 'App\Models\Product';

    public array $relations = [
        'langs'
    ];

    public function getByParamsPaginated($params, $qty)
    {
        return Product::where($params)->paginate($qty);
    }
    public function getByIdsPaginated($ids, $qty)
    {
        return Product::whereIn('id', $ids)->paginate($qty);
    }
    public function getByIds($ids, $qty)
    {
        return Product::whereIn('id', $ids)->limit($qty)->get();
    }

}
