<?php
namespace App\Repository\Formatting;

use App\Models\AttributeDesc;
use App\Models\AttributeValueDesc;
use App\Models\Product;

trait ELProduct
{
    public function instance($id)
    {
        $product = Product::where('id', $id)->with($this->relations)->first();

        foreach ($product->attributes as $val) {

            if($val->value->attribute_id !=1){
                $id = "atr_" . $val->value->attribute_id;
                $name_lang = [];
                $val_lang = [];
                foreach ($val->value->langs as $name) {
                    $val_lang[$name->lang] = $name->name;
                }
                foreach ($val->value->attribute->langs as $name) {
                    $name_lang[$name->lang] = $name->name;
                }
                $product->$id = ['id' => $val->value->attribute_id, 'name' => $name_lang, 'value_id' => $val->attribute_val_id, 'value' => $val_lang];
            }
        }

        $attr = [];
        if ($val->value ?? '') {
            // цикл всех языков
            foreach ($val->value->langs as $lang) {
                $attribute = [];

                //цикл всех атрибутов товара
                foreach ($product->attributes as $val) {

                    if($val->value->attribute_id !=1){
                        $attr_desc = AttributeDesc::where('lang', $lang->lang)->where('attribute_id', $val->value->attribute_id)->first();
                        $val_desc = AttributeValueDesc::where('lang', $lang->lang)->where('attribute_val_id', $val->attribute_val_id)->first();

                        if ($attr_desc && $val_desc) {
                            $attribute['id'] = $val->value->attribute_id;
                            $attribute['name'] = $attr_desc->name;
                            $attribute['value_id'] = $val->attribute_val_id;
                            $attribute['value'] = $val_desc->name;
                            $attribute['value_name'] = $val->attribute_val_id ."*$*".$val_desc->name;
                            $attribute['attribute_name'] = $val->value->attribute_id ."*$*". $attr_desc->name;
                            $attribute['atr_val'] = $val->value->attribute_id ."*$*". $val->attribute_val_id;

                        }
                        $attr[$lang->lang][] = $attribute;
                    }

                }

            }
        }
        $product->attr = $attr;
        return $product;
    }
}
