<?php

namespace App\Repository;

use App\Models\Category;

class CategoryRepository extends AbstractRepository
{
    const MODEL = 'App\Models\Category';

    public array $relations = [
        'lang', 'products', 'children.lang', 'children.children'
    ];

    public function main()
    {
        return Category::where('id', 1)->with($this->relations)->first();
    }
    public function menu_categories()
    {
        return Category::where('parent_id', 0)->with($this->relations)->get();
    }
}
