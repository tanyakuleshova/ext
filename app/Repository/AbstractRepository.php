<?php

namespace App\Repository;

abstract class AbstractRepository
{
    public function getAll()
    {
        return static::MODEL::all();
    }

    public function getById($id)
    {
        return static::MODEL::where('id', $id)->with($this->relations)->first();
    }

    public function delete($id)
    {
        return static::MODEL::where('id', $id)->delete();
    }
}

